#!/usr/bin/env python 

import rospy
from dynamixel_workbench_msgs.srv import DynamixelCommand

def move_arm():
    rospy.wait_for_service("/dynamixel_workbench/dynamixel_command")
    move_arm = rospy.ServiceProxy("/dynamixel_workbench/dynamixel_command",DynamixelCommand)
    move_arm("",0,"Goal_Position",2048)
    move_arm("",1,"Goal_Position",2408)
    move_arm("",2,"Goal_Position",2492)
    rospy.sleep(2)
    move_arm("",0,"Goal_Position",1807)
    rospy.sleep(0.5)
    move_arm("",0,"Goal_Position",2048)
    rospy.sleep(0.5)
    move_arm("",0,"Goal_Position",1807)
    rospy.sleep(0.5)
    move_arm("",0,"Goal_Position",2048)
    rospy.sleep(0.5)    
    move_arm("",0,"Goal_Position",1807)
    move_arm("",1,"Goal_Position",3254)
    move_arm("",2,"Goal_Position",1694)

    
# if __name__ == '__main__':
#     rospy.init_node("move_arm_service",anonymous=True)
#     move_arm()